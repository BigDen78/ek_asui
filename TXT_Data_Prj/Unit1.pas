unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Src: TStringList;
  Out_: TStringList;
  fL: TStringList;

  RR: TStringList;
  PRED: TStringList;
  PNAME: TStringList;
  CODE: TStringList;
  ID: TStringList;
  PATH: TStringList;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  I, J, MaxIdx: Integer;
  Flag: Boolean;

function Test1(Idx1, Idx2: Integer): Integer;
begin
  if RR[Idx1] < RR[Idx2] then Result:= - 1 else
  if RR[Idx1] > RR[Idx2] then Result:= + 1 else
  if PNAME[Idx1] < PNAME[Idx2] then Result:= - 1 else
  if PNAME[Idx1] > PNAME[Idx2] then Result:= + 1 else Result:= 0;
end;

function Test2(Idx1, Idx2: Integer): Integer;
begin
  if RR[Idx1] < RR[Idx2] then Result:= - 1 else
  if RR[Idx1] > RR[Idx2] then Result:= + 1 else
  if CODE[Idx1] < CODE[Idx2] then Result:= - 1 else
  if CODE[Idx1] > CODE[Idx2] then Result:= + 1 else Result:= 0;
end;


function Change1(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  PNAME_: string;

begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
  PNAME_:= PNAME[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
  PNAME[Idx1]:= PNAME[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
  PNAME[Idx2]:= PNAME_;
end;

function Change2(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  PNAME_: string;
  CODE_: string;

begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
  PNAME_:= PNAME[Idx1];
  CODE_:= CODE[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
  PNAME[Idx1]:= PNAME[Idx2];
  CODE[Idx1]:= CODE[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
  PNAME[Idx2]:= PNAME_;
  CODE[Idx2]:= CODE_;
end;

function Change3(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  PNAME_: string;
  CODE_: string;
  ID_: string;
  PATH_: string;

begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
//  PNAME_:= PNAME[Idx1];
  CODE_:= CODE[Idx1];
  ID_:= ID[Idx1];
  PATH_:= PATH[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
//  PNAME[Idx1]:= PNAME[Idx2];
  CODE[Idx1]:= CODE[Idx2];
  ID[Idx1]:= ID[Idx2];
  PATH[Idx1]:= PATH[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
//  PNAME[Idx2]:= PNAME_;
  CODE[Idx2]:= CODE_;
  ID[Idx2]:= ID_;
  PATH[Idx2]:= PATH_;
end;



begin

// �������� ������������� ����
(*
  Src:= TStringList.Create;
 // Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPaths.txt');
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPaths_(ansi).txt');
 // Self.Caption:= Src[0];

  MaxIdx:= Src.Count - 1;
  for I := MaxIdx downto 0 do
    for J := I - 1 downto 0 do
      if Src[I] = Src[J] then
      begin
        Memo1.Lines.Add(Format('%d %d', [I, J]));
        Src.Delete(I);
        Break;
      end;
  Src.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPaths_Filtr(ansi).txt');
 *)

(*
 // -----------------------------------------------------------------------------------
 // ������ ��
  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPaths_Filtr(ansi).txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  PNAME:= TStringList.Create;
  fL := TStringList.Create;
  for I := 0 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 7 then Continue;

    if (PRED.IndexOf(fL[1]) = -1) and (fL[1] <> '') then
    begin
      RR.Add(fL[0]);
      PRED.Add(fL[1]);
      PNAME.Add(fL[2]);
    end;
  end;

  // ����������
  repeat
    Flag:= False;
    for I := 0 to RR.Count - 2 do
      if Test1(I, I + 1) > 0 then
      begin
        Flag:= True;
        Change1(I, I + 1);
      end;
  until not Flag;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s', [RR[I], PRED[I], PNAME[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\PRED.txt');
*)

 // -----------------------------------------------------------------------------------
 // �����������
  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPaths_Filtr(ansi).txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  PNAME:= TStringList.Create;
  CODE:= TStringList.Create;

  fL := TStringList.Create;
  for I := 0 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 7 then Continue;

    if (CODE.IndexOf(fL[3]) = -1) or
       ((CODE.IndexOf(fL[3]) <> -1) and (PRED.IndexOf(fL[1]) = -1)) then
    begin
      RR.Add(fL[0]);
      PRED.Add(fL[1]);
      CODE.Add(fL[3]);
      PNAME.Add(fL[4]);
    end;
  end;

  // ����������
  repeat
    Flag:= False;
    for I := 0 to RR.Count - 2 do
      if Test1(I, I + 1) > 0 then
      begin
        Flag:= True;
        Change2(I, I + 1);
      end;
  until not Flag;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s', [RR[I], CODE[I], PNAME[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\DIRECTIONS.txt');

(*
 // -----------------------------------------------------------------------------------
 // ��. ����
  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPaths_Filtr(ansi).txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  PNAME:= TStringList.Create;
  CODE:= TStringList.Create;
  ID:= TStringList.Create;
  PATH:= TStringList.Create;

  fL := TStringList.Create;
  for I := 0 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 7 then Continue;

    if (ID.IndexOf(fL[5]) = -1) or
       ((ID.IndexOf(fL[5]) <> -1) and (PATH.IndexOf(fL[6]) = -1)) then
    begin
      RR.Add(fL[0]);
      PRED.Add(fL[1]);
      CODE.Add(fL[3]);
    //  PNAME.Add(fL[4]);
      ID.Add(fL[5]);
      PATH.Add(fL[6]);
    end;
  end;

  // ����������
  repeat
    Flag:= False;
    for I := 0 to RR.Count - 2 do
      if Test2(I, I + 1) > 0 then
      begin
        Flag:= True;
        Change3(I, I + 1);
      end;
  until not Flag;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s;%s', [RR[I], CODE[I], PATH[I], ID[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\MainPath.txt');
   *)
end;

end.
