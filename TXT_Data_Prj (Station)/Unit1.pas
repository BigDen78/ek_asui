unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Src: TStringList;
  Out_: TStringList;
  fL: TStringList;

  RR: TStringList;
  PRED: TStringList;
  STAN: TStringList;
  SNAME: TStringList;
  ID: TStringList;
  PATH: TStringList;
  PARK: TStringList;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  I, J, MaxIdx: Integer;
  Flag: Boolean;
  str: string;
  str2: string;

function Test1(Idx1, Idx2: Integer): Integer;
begin
  if RR[Idx1] < RR[Idx2] then Result:= - 1 else
  if RR[Idx1] > RR[Idx2] then Result:= + 1 else
  if SNAME[Idx1] < SNAME[Idx2] then Result:= - 1 else
  if SNAME[Idx1] > SNAME[Idx2] then Result:= + 1 else Result:= 0;
end;

function Test2(Idx1, Idx2: Integer): Integer;
begin
  if RR[Idx1] < RR[Idx2] then Result:= - 1 else
  if RR[Idx1] > RR[Idx2] then Result:= + 1 else
  if PATH[Idx1] < PATH[Idx2] then Result:= - 1 else
  if PATH[Idx1] > PATH[Idx2] then Result:= + 1 else Result:= 0;
end;

function Test3(Idx1, Idx2: Integer): Integer;
begin
  if RR[Idx1] < RR[Idx2] then Result:= - 1 else
  if RR[Idx1] > RR[Idx2] then Result:= + 1 else
end;

function Test4(Idx1, Idx2: Integer): Integer;
begin
  if RR[Idx1] < RR[Idx2] then Result:= - 1 else
  if RR[Idx1] > RR[Idx2] then Result:= + 1 else
  if PATH[Idx1] + PARK[Idx1] < PATH[Idx2] + PARK[Idx2] then Result:= - 1 else
  if PATH[Idx1] + PARK[Idx1] > PATH[Idx2] + PARK[Idx2] then Result:= + 1 else Result:= 0;
end;

function Change1(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  SNAME_: string;
  STAN_: string;

begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
  SNAME_:= SNAME[Idx1];
  STAN_:= STAN[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
  SNAME[Idx1]:= SNAME[Idx2];
  STAN[Idx1]:= STAN[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
  SNAME[Idx2]:= SNAME_;
  STAN[Idx2]:= STAN_;
end;
     {
function Change2(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  PNAME_: string;
  CODE_: string;

begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
  PNAME_:= PNAME[Idx1];
  CODE_:= CODE[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
  PNAME[Idx1]:= PNAME[Idx2];
  CODE[Idx1]:= CODE[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
  PNAME[Idx2]:= PNAME_;
  CODE[Idx2]:= CODE_;
end;  }

function Change3(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  SNAME_: string;
  STAN_: string;
  ID_: string;
  PATH_: string;


begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
  SNAME_:= SNAME[Idx1];
  STAN_:= STAN[Idx1];
//  ID_:= ID[Idx1];
//  PATH_:= PATH[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
  SNAME[Idx1]:= SNAME[Idx2];
  STAN[Idx1]:= STAN[Idx2];
//  ID[Idx1]:= ID[Idx2];
//  PATH[Idx1]:= PATH[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
  SNAME[Idx2]:= SNAME_;
  STAN[Idx2]:= STAN_;
//  ID[Idx2]:= ID_;
//  PATH[Idx2]:= PATH_;
end;

function Change4(Idx1, Idx2: Integer): Integer;
var
  RR_: string;
  PRED_: string;
  SNAME_: string;
  STAN_: string;
  ID_: string;
  PATH_: string;
  PARK_: string;

begin
  RR_:= RR[Idx1];
  PRED_:= PRED[Idx1];
  STAN_:= STAN[Idx1];
  ID_:= ID[Idx1];
  PATH_:= PATH[Idx1];
  PARK_:= PARK[Idx1];

  RR[Idx1]   := RR[Idx2];
  PRED[Idx1] := PRED[Idx2];
  STAN[Idx1]:= STAN[Idx2];
  ID[Idx1]:= ID[Idx2];
  PATH[Idx1]:= PATH[Idx2];
  PARK[Idx1]:= PARK[Idx2];

  RR[Idx2]   := RR_;
  PRED[Idx2] := PRED_;
  STAN[Idx2]:= STAN_;
  ID[Idx2]:= ID_;
  PATH[Idx2]:= PATH_;
  PARK[Idx2]:= PARK_;
end;

begin

// -----------------------------------------------------------------------------------
// �������� ������������� ����
   (*
  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\�������� ��-��������-������.csv');

  MaxIdx:= Src.Count - 1;
  for I := MaxIdx downto 0 do
    for J := I - 1 downto 0 do
      if Src[I] = Src[J] then
      begin
        Memo1.Lines.Add(Format('%d %d', [I, J]));
        Src.Delete(I);
        Break;
      end;

  Src.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj\STATION_ALL(ansi).txt');
  *)
// -----------------------------------------------------------------------------------

// ������������
(*
  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_ALL(ansi).txt');

  for I := 0 to Src.Count - 1 do
  begin
    J:= Pos(' �� ������� ', Src[I]);
    if J <> 0 then
    begin
      str:= Src[I];
      str2:= Src[I];
      Delete(str, J, 255);
      Src[I]:= str + ';';
      Delete(str2, 1, J + 11);
      Src[I]:= Src[I] + str2;
    end;
  end;

  Src.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_!(ansi).txt');
*)
(*
 // -----------------------------------------------------------------------------------
 // ������ �������

  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_!(ansi).txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  STAN:= TStringList.Create;
  SNAME:= TStringList.Create;
  ID:= TStringList.Create;
  PATH:= TStringList.Create;


  fL := TStringList.Create;
  for I := 1 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 7 then Continue;

    if (SNAME.IndexOf(fL[6]) = - 1) or
       ((SNAME.IndexOf(fL[6]) <> - 1) and (STAN.IndexOf(fL[2]) = - 1)) then
    begin
      RR.Add(fL[0]);
      PRED.Add(fL[1]);
      STAN.Add(fL[2]);

      ID.Add(fL[4]);
      PATH.Add(fL[5]);
      SNAME.Add(fL[6]);
    end;
  end;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s', [RR[I], STAN[I], SNAME[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATIONs.txt');
*)
 // -----------------------------------------------------------------------------------
 // ������ �������  - ����������
(*
  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATIONs.txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  STAN:= TStringList.Create;
  SNAME:= TStringList.Create;
  ID:= TStringList.Create;
  PATH:= TStringList.Create;

  fL := TStringList.Create;
  for I := 1 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    RR.Add(fL[0]);
    STAN.Add(fL[1]);
    SNAME.Add(fL[2]);
    PRED.Add(fL[3]);
  end;

  // ����������
  repeat
    Flag:= False;
    for I := 0 to RR.Count - 2 do
      if Test1(I, I + 1) > 0 then
      begin
        Flag:= True;
        Change3(I, I + 1);
      end;
  until not Flag;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s', [RR[I], STAN[I], SNAME[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATIONs_sort.txt');
 *)
(*
  // -----------------------------------------------------------------------------------
 // ����������� ���� - ������������

  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_!(ansi).txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  STAN:= TStringList.Create;
  ID:= TStringList.Create;
  PATH:= TStringList.Create;
  SNAME:= TStringList.Create;
  PARK:= TStringList.Create;

  fL := TStringList.Create;
  for I := 1 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 7 then Continue;

    begin
      RR.Add(fL[0]);
      PRED.Add(fL[1]);
      STAN.Add(fL[2]);
      ID.Add(fL[4]);
      str:= fL[5];
      J:= Pos(' � ����� ', str);
      if J <> 0 then
      begin
        str2:= Copy(str, J + 9, 255);
        Delete(str, J, 255);
      end else str2:= '';
      PATH.Add(str);
      PARK.Add(str2);
      SNAME.Add(fL[6]);
    end;
  end;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s;%s;%s', [RR[I], PATH[I], PARK[I], STAN[I], ID[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_PATHs_no_sort.txt');
*)
(*
 // -----------------------------------------------------------------------------------
 // ����������� ���� - ���������� RR

  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_PATHs_no_sort.txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  STAN:= TStringList.Create;
  ID:= TStringList.Create;
  PATH:= TStringList.Create;
  SNAME:= TStringList.Create;
  PARK:= TStringList.Create;

  fL := TStringList.Create;
  for I := 1 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 6 then Continue;

    RR.Add(fL[0]);
    PATH.Add(fL[1]);
    PARK.Add(fL[2]);
    STAN.Add(fL[3]);
    ID.Add(fL[4]);
    PRED.Add(fL[5]);
  end;

  // ����������
  repeat
    Flag:= False;
    for I := 0 to RR.Count - 2 do
      if Test3(I, I + 1) > 0 then
      begin
        Flag:= True;
        Change4(I, I + 1);
      end;
  until not Flag;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s;%s;%s', [RR[I], PATH[I], PARK[I], STAN[I], ID[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_PATHs_sort_RR.txt');
*)
 // -----------------------------------------------------------------------------------
 // ����������� ���� - ���������� RR+����+����

  Src:= TStringList.Create;
  Src.LoadFromFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_PATHs_sort_RR.txt');

  RR:= TStringList.Create;
  PRED:= TStringList.Create;
  STAN:= TStringList.Create;
  ID:= TStringList.Create;
  PATH:= TStringList.Create;
  SNAME:= TStringList.Create;
  PARK:= TStringList.Create;

  fL := TStringList.Create;
  for I := 1 to Src.Count - 1 do
  begin
    fL.Delimiter := ';';
    fL.StrictDelimiter := True;
    fL.DelimitedText := Src[I];

    if fL.Count <> 6 then Continue;

    RR.Add(fL[0]);
    PATH.Add(fL[1]);
    PARK.Add(fL[2]);
    STAN.Add(fL[3]);
    ID.Add(fL[4]);
    PRED.Add(fL[5]);
  end;

  // ����������
  repeat
    Flag:= False;
    for I := 0 to RR.Count - 2 do
      if Test4(I, I + 1) > 0 then
      begin
        Flag:= True;
        Change4(I, I + 1);
      end;
  until not Flag;

  // ����������
  Out_:= TStringList.Create;
  for I := 0 to RR.Count - 1 do
    Out_.Add(Format('%s;%s;%s;%s;%s;%s', [RR[I], PATH[I], PARK[I], STAN[I], ID[I], PRED[I]]));

  Out_.SaveToFile('C:\Denis\work\Active\EKASUI\TXT_Data_Prj (Station)\DATA\STATION_PATHs_sort_RR_PP.txt');

end;

end.
