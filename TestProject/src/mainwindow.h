#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include "D:\work\Projects\defcore\ekasui\EK_ASUIdata.h"
#include "C:\Denis\work\Active\Projects\defcore\ekasui\EK_ASUIdata.h"
#include <QtXml>
#include <QtBluetooth\QBluetoothLocalDevice>
#include <QtBluetooth\QBluetoothServiceDiscoveryAgent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    EK_ASUI * as;
    QStringList *PODRs;
    QStringList NAPRs;
    QStringList STANs;
    //TextDataSource * Directions;
    //TextDataSource * MainPath;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_Dircb_activated(const QString &arg1); //

    void on_Stations_activated(const QString &arg1); //

    void on_RRcb_activated(const QString &arg1); //

    void on_PREDcb_activated(const QString &arg1); //


    void on_pushButton_released();

    void on_cbMainPath_stateChanged(int arg1);

    void on_cbStationPath_stateChanged(int arg1);

    void on_Defects_cb_activated(const QString &arg1);

    void on_MainPath_cb_activated(const QString &arg1);

    void on_StationPath_cb_activated(const QString &arg1);

    void on_pushButton_3_clicked();


    void on_PREDcb_currentIndexChanged(const QString &arg1);

    void on_pushButton_2_released();

    void on_pushButton_4_released();

private:
    bool update;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
