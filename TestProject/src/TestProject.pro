#-------------------------------------------------
#
# Project created by QtCreator 2017-06-01T17:48:57
#
#-------------------------------------------------

QT       += core gui
QT += xml
QT += sql
QT += bluetooth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestProject
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
#    D:\work\Projects\defcore\ekasui\EK_ASUIdata.cpp
    C:\Denis\work\Active\Projects\defcore\ekasui\EK_ASUIdata.cpp

HEADERS  += mainwindow.h \
#    D:\work\Projects\defcore\ekasui\EK_ASUIdata.h
    C:\Denis\work\Active\Projects\defcore\ekasui\EK_ASUIdata.h

FORMS    += mainwindow.ui
