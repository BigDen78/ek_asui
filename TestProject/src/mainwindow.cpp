#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    as = new EK_ASUI(QApplication::applicationDirPath(), true);
    PODRs = new QStringList();

    ui->RRcb->addItems(as->GetRRs());
    ui->RRcb->setCurrentIndex(0);

    ui->Defects_cb->addItems(as->GetDEFECTshortNames());
    ui->Defects_cb->setCurrentIndex(0);

}

MainWindow::~MainWindow()
{
    delete ui;
}

// ------------------------------------------------------------------ //

// Пункт №1
// Выбрана Дороги - заполняем список ПЧ
void MainWindow::on_RRcb_activated(const QString &arg1)
{
    ui->PREDcb->clear();
    ui->PREDcb->addItems(as->GetPREDs_by_RR(arg1, PODRs));
}

// Пункт №2
// Выбрано ПЧ - заполняем список Направлений и Станций
void MainWindow::on_PREDcb_activated(const QString &arg1)
{
//    as->LoadData(ui->RRcb->currentText(), arg1);

    ui->Dircb->clear();
    QString tmp = PODRs->at(ui->PREDcb->currentIndex());
    ui->Dircb->addItems(as->Get_DIRECTIONS_NAME_by_PODR(tmp));
    NAPRs = as->Get_DIRECTIONS_NAPR_by_PODR(tmp);

    ui->Dircb->setCurrentIndex(0);

//    ui->Dircb->clear();
//    ui->Dircb->addItems(as->GetDIRNames_by_RR_PRED(ui->RRcb->currentText(), arg1));
//    ui->Dircb->setCurrentIndex(0);


    STANs = as->Get_STATIONS_STAN_by_PODR(tmp);
    ui->Stations->clear();
    ui->Stations->addItems(as->Get_STATIONS_NAME_by_PODR(tmp));
    ui->Stations->setCurrentIndex(0);
}

// Пункт №3a
// Для режима контроля главных путей
// Выбор направления - заполняем список путей: идентификатор, номер пути
void MainWindow::on_Dircb_activated(const QString &arg1)
{



//    QStringList IDs = as->GetMAINPATH_IDs_by_RR_PRED_DIR(ui->RRcb->currentText(), ui->PREDcb->currentText(), arg1);
//    QStringList NPUTs = as->GetMAINPATH_NPUTs_by_RR_PRED_DIR(ui->RRcb->currentText(), ui->PREDcb->currentText(), arg1);

    QStringList IDs = as->Get_IDPUT_by_NAPR(NAPRs.at(ui->Dircb->currentIndex()));
    QStringList NPUTs = as->Get_NPUT_by_NAPR(NAPRs.at(ui->Dircb->currentIndex()));


    ui->lineEdit_2->setText(NAPRs.at(ui->Dircb->currentIndex()));

    ui->MainPath_cb->clear();
    for (unsigned int idx = 0; idx < IDs.size(); idx++)
        ui->MainPath_cb->addItem(/*"id: " + IDs[idx] + " path: " +*/ NPUTs[idx]);
    ui->MainPath_cb->setCurrentIndex(0);
}

// Выбран путь
void MainWindow::on_MainPath_cb_activated(const QString &arg1)
{
    QStringList IDs = as->Get_IDPUT_by_NAPR(NAPRs.at(ui->Dircb->currentIndex()));
    QStringList NPUTs = as->Get_NPUT_by_NAPR(NAPRs.at(ui->Dircb->currentIndex()));

    int idx = NPUTs.indexOf(arg1);
    if (idx != -1) ui->lineEdit->setText(IDs[idx]);
}

// Пункт №3b
// Для режима контроля станционных путей
// Выбор станции - заполняем список путей: идентификатор, номер пути, парк
void MainWindow::on_Stations_activated(const QString &arg1)
{
    QStringList IDs = as->Get_IDPUT_by_STAN(STANs.at(ui->Stations->currentIndex()));
    QStringList NPUTs = as->Get_NAME_by_STAN(STANs.at(ui->Stations->currentIndex()));
    QStringList PARKs = as->Get_PARKNAME_by_STAN(STANs.at(ui->Stations->currentIndex()));

    ui->StationPath_cb->clear();
    for (unsigned int idx = 0; idx < IDs.size(); idx++)
        ui->StationPath_cb->addItem(/*"id: " + IDs[idx] + " path: " +*/ NPUTs[idx] /*+ " park: " + PARKs[idx]*/);
    ui->StationPath_cb->setCurrentIndex(0);

    ui->lineEdit_3->setText(as->GetSTATIONcode_by_Name(arg1));
}

// Пункт №4b
// Для режима контроля станционных путей
// Выбор пути станции
void MainWindow::on_StationPath_cb_activated(const QString &arg1)
{
    QStringList IDs = as->Get_IDPUT_by_STAN(STANs.at(ui->Stations->currentIndex()));
    QStringList NPUTs = as->Get_NAME_by_STAN(STANs.at(ui->Stations->currentIndex()));
    QStringList PARKs = as->Get_PARKNAME_by_STAN(STANs.at(ui->Stations->currentIndex()));

    QStringList list;
    int idx = NPUTs.indexOf(arg1);
    if (idx != -1) {

        list.append(IDs[idx]);
        list.append(PARKs[idx]);
    }

    //return list;

    ui->DefectInfo_2->clear();
    ui->DefectInfo_2->append(list[0]);
    ui->DefectInfo_2->append(list[1]);
}

void MainWindow::on_cbMainPath_stateChanged(int arg1)
{
    ui->cbStationPath->setChecked(!ui->cbMainPath->checkState());
}

void MainWindow::on_cbStationPath_stateChanged(int arg1)
{
    ui->cbMainPath->setChecked(!ui->cbStationPath->checkState());
}

void MainWindow::on_Defects_cb_activated(const QString &arg1)
{
    QStringList res = as->GetDEFECTdataBYshortName(arg1);
    ui->DefectInfo->clear();
    ui->DefectInfo->append(res[0]);
    ui->DefectInfo->append(res[1]);
    ui->DefectInfo->append(res[2]);
}

void MainWindow::on_pushButton_3_clicked()
{
    QDateTime dt = QDateTime::currentDateTime();
    ui->lineEdit_4->setText(dt.toString("yyyyMMddhhmmsszzz"));
}

void MainWindow::on_pushButton_released()
{

    //QString fn = "C:/Denis/work/Active/Files/A31/OLD/photo/150915-130103.png";
    QString fn = "D:/work/Files/Авикон-31/photo/150915-130103.png";
    QFile *TF = new QFile(fn);
    if (!TF->open(QIODevice::ReadOnly)) return;

    void *tmp = malloc(TF->size());
    TF->read((char *)tmp, TF->size());



    if (ui->cbMainPath->checkState()  == Qt::Checked /* CheckState::Checked*/) {

        as->CreateMainPathsIncidentXML("D:/work/EK_ASUI/TestProject/test.xml",  // Полное имя создоваемого файла
                                       //"C:/Denis/work/Active/EKASUI/TestProject/build-TestProject-Desktop_Qt_5_7_0_MinGW_32bit-Debug/debug/test-mp.xml",
                                       "v0.3.389", // Версия ПО БУИ [+]
                                       "Похоруков", // Оператор/расшифровщик [+]
                                       ui->RRcb->currentText(), // Название Ж/Д  [+]
                                       ui->PREDcb->currentText(), // Название ПЧ  [+]
                                       ui->Dircb->currentText(), // Название направления [+ ГП]
                                       ui->MainPath_cb->currentText(), // Номер пути [+]
                                       11, // Длина дефекта [+]
                                       22, // Глубина дефекта [+]
                                       33, // Ширина дефекта [+]
                                       44, // Ограничение скорости [+]
                                       10, // Километр [+]
                                       5, // Пикет [+]
                                       55, // Метр [+]
                                       "53.1", // Код дефекта [+][+]
                                       "Проверка XML", // Коментарий [+]
                                       "8 этаж", // Привязка [+]
                                       1, // Нить пути: 0-Левая; 1-Правая [+]
                                       tmp, // Изображение дефекта - указатель на изображение в формате PNG
                                       TF->size(),
                                       fn,
                                       55.800834,
                                       37.619270);
    }
    else
    {
        as->CreateStationIncidentXML("D:/work/EK_ASUI/TestProject/test.xml",  // Полное имя создоваемого файла
                                     //"C:/Denis/work/Active/EKASUI/TestProject/build-TestProject-Desktop_Qt_5_7_0_MinGW_32bit-Debug/debug/test-st.xml",
                                     "v0.3.389", // Версия ПО БУИ [+]
                                     "Похоруков", // Оператор/расшифровщик [+]
                                     ui->RRcb->currentText(), // Название Ж/Д  [+]
                                     ui->PREDcb->currentText(), // Название ПЧ  [+]
                                     ui->Stations->currentText(), // Название станции [+ ГП]
                                     ui->StationPath_cb->currentText(), // Номер пути [+]
                                     11, // Длина дефекта [+]
                                     22, // Глубина дефекта [+]
                                     33, // Ширина дефекта [+]
                                     44, // Ограничение скорости [+]
                                     10, // Километр [+]
                                     5, // Пикет [+]
                                     55, // Метр [+]
                                     "53.1", // Код дефекта [+][+]
                                     "Проверка XML", // Коментарий [+]
                                     "8 этаж", // Привязка [+]
                                     1, // Нить пути: 0-Левая; 1-Правая [+]
                                     tmp, // Изображение дефекта - указатель на изображение в формате PNG
                                     TF->size(),
                                     fn,
                                     55.800834,
                                     37.619270);

    }
    free(tmp);
    delete TF;
}

void MainWindow::on_PREDcb_currentIndexChanged(const QString &arg1)
{

}

void MainWindow::on_pushButton_2_released()
{
    as->UpdateDB();
}

void MainWindow::on_pushButton_4_released()
{
    /*
    QBluetoothLocalDevice * localDevice = new QBluetoothLocalDevice(this);

    if (localDevice->isValid()) {

        qDebug() << "Bluetooth is available on this device";
        localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        localDevice->powerOn();
        localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        qDebug() << "Local device: " << localDevice->name() << " ("
                 << localDevice->address().toString().trimmed() << ")";
        // Create a discovery agent and connect to its signals
        QBluetoothServiceDiscoveryAgent * discoveryAgent = new QBluetoothServiceDiscoveryAgent(this);
        connect(discoveryAgent, SIGNAL(finished()),
                this, SLOT(serviceDiscoverFinished()));
        discoveryAgent->start();
        qDebug() << "Service discover started";
    }
    else
        qDebug() << "Bluetooth is not available on this device";
        */
}
